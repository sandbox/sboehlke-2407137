== REQUIREMENTS ==
******************

* jQuery Update
  http://drupal.org/project/jquery_update
  
* Libraries API
  http://drupal.org/project/libraries
  
* Shariff Library
  https://github.com/heiseonline/shariff


== INSTALLATION ==
******************

1) Download the library from https://github.com/heiseonline/shariff and
extract this file in your libraries folder (sites/all/libraries). The name
of the folder containing all the files must be "shariff", e.g.
"sites/all/libraries/shariff".

2) Activate the module and configure it under
admin/config/user-interface/ct-shariff. At least one service must be activated
to have some kind of output.

3) Now there is a new field type available called "c't Shariff" which you can
add to your Content Types.
