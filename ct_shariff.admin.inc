<?php

/**
 * @file
 * Contains administration forms.
 */

/**
 * General configuration form for controlling the behaviour.
 */
function ct_shariff_admin_settings() {
  $form['ct_shariff_theme'] = array(
    '#title'       => t('Theme'),
    '#description' => t('Please select a color scheme.'),
    '#type'        => 'radios',
    '#options'     => array(
      'color' => t('Color'),
      'grey'  => t('Grey'),
      'white' => t('White'),
    ),
    '#default_value' => variable_get('ct_shariff_theme', 'color'),
  );

  $form['ct_shariff_orientation'] = array(
    '#title'       => t('Orientation'),
    '#description' => t('Orientation for sharing buttons.'),
    '#type'        => 'radios',
    '#options'     => array(
      'horizontal' => t('Horizontal'),
      'vertical'   => t('Vertical'),
    ),
    '#default_value' => variable_get('ct_shariff_orientation', 'horizontal'),
  );

  $form['ct_shariff_services'] = array(
    '#title'       => t('Services'),
    '#description' => t('Enabled services for sharing buttons.'),
    '#type'        => 'checkboxes',
    '#options'     => array(
      'facebook'   => t('Facebook'),
      'twitter'    => t('Twitter'),
      'googleplus' => t('Google+'),
      'whatsapp'   => t('WhatsApp'),
      'mail'       => t('E-Mail'),
      'info'       => t('Info'),
    ),
    '#default_value' => variable_get('ct_shariff_services',
      array('facebook', 'twitter', 'googleplus')),
  );

  $form['ct_shariff_backend_path'] = array(
    '#title'         => t('Backend Path'),
    '#description'   => t('The path to the Shariff backend.'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('ct_shariff_backend_path', ''),
  );

  $form['#submit']   = array('ct_shariff_admin_settings_form_submit');

  return system_settings_form($form);
}


/**
 * C't shariff settings submit handler.
 */
function ct_shariff_admin_settings_form_submit($form, &$form_state) {
  foreach ($form_state['values']['ct_shariff_services'] as $key => $value) {
    if (empty($value)) {
      unset($form_state['values']['ct_shariff_services'][$key]);
    }
  }
}
